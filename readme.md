# scripttricks.js

### What is this

Some javascript and css hacks for goodschoolplays.com. If you think it would be useful to you, please use it.

### Does three things:
1. Numbers lines in a script. This should be helpful for plays published online, because it's tricky to give a mobile-friendly script meaningful page numbers.
2. Counts lines belonging to each character. This is intended to be handy for theater teachers who want to get a quick idea of how demanding each part is.
3. Produces a section containing a Javascript button for each character. When pressed, the button highlights each line belonging to its character.

### How to use

In short: make your file look just like demo.html.

* It needs some stuff from the included ,css file to work correctly.
* The play has to be inside a div whose id is "play".
* Each character's name has to be inside a span of class "characterName".
* Each line has to be inside a div of class "line".
* Put a div with id "buttons" where you want the highlighter buttons to go.
* Put a div with id "cueCounts" where you want the lines-per-character counts to go.