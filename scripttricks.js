var LineNumberer = (function() {
    var publicInterface = {};

    var extractDomAsTable = function(domObject) {
        var table = document.createElement('table');

        while (domObject.childElementCount > 0) {
            if (domObject.firstChild.innerHTML) {
                var tr = table.insertRow();
                var td = tr.insertCell();
                td.appendChild(domObject.firstChild);
            } else {
                domObject.removeChild(domObject.firstChild);
            }
        }

        return table;
    }

    var insertLineNumberCells = function(table) {
        var nextLineNumber = 1;

        var rows = table.rows;

        for (var i = 0; i < rows.length; i++) {
            var currentRow = rows[i];
            var indexCell = currentRow.insertCell(0)

            if (currentRow.cells[1].firstChild.className === "line") {
                indexCell.innerHTML = nextLineNumber++;
                indexCell.className = 'lineNumber';
            }
        }

        return table;
    };

    var numberLines = function() {
        var playDomObject = document.getElementById("play");

        if (!playDomObject) {
            console.error("The play needs to be in a div of class 'play'");
            return;
        }

        var table = extractDomAsTable(playDomObject);
        insertLineNumberCells(table);
        playDomObject.appendChild(table);
    };

    publicInterface.numberLines = numberLines;

    return publicInterface;
})();

var HighlightButtonMaker = (function() {
    var publicInterface = {};

    var createCharacterHighlighterButtons = function() {
        var characterNames = getCharacterNames();

        var buttonDOM = document.getElementById("buttons");

        if (!buttonDOM) {
            console.error("building line highlighters failed; " +
                          "i need a div vith id 'buttons' to make line " +
                          "highlighting buttons");

            return;
        }

        for (var i = 0; i < characterNames.length; i++) {
            var name = characterNames[i];
            var button = document.createElement("button");
            var textNode = document.createElement("span");
            textNode.innerText = name;
            textNode.className = "characterName";
            button.appendChild(textNode);
            button.addEventListener("click", (function(name) {
                return function() {
                    this.classList.toggle('highlighted');
                    toggleLineHighlightingForCharacter(name)
                };
            })(name));
            buttonDOM.appendChild(button);
        }
    };

    var highlightCharacterLines = function(characterName) {
        var characterNamesWithinLines = getCharacterNamesWithinLines();

        for (var i = 0; i < characterNamesWithinLines.length; i++) {
            if (characterNamesWithinLines[i].innerText.toUpperCase() ===
                characterName.toUpperCase()) {
                characterNamesWithinLines[i].parentNode.classList.add(
                    "highlighted");
            }
        }
    };

    var unHighlightCharacterLines = function(characterName) {
        var charactersNamesWithinLines = getCharacterNamesWithinLines();

        for (var i = 0; i < charactersNamesWithinLines.length; i++) {
            if (charactersNamesWithinLines[i].innerText.toUpperCase() ===
                characterName.toUpperCase()) {
                charactersNamesWithinLines[i].parentNode.classList.remove(
                    "highlighted");
            }
        }
    };

    var getCharacterNamesWithinLines = function() {
        var characterNamesDOMNodes =
            document.querySelectorAll('.line>.characterName');

        if (characterNamesDOMNodes.length === 0) {
            console.error("building line numbers failed; " +
                          "each character's name should be directly inside a " +
                          "span of class 'characterName', and eadh line " +
                          "should be inside a div of class 'line' that " +
                          "includes a characterName node");
        };

        return characterNamesDOMNodes;
    }

    var getCharacterNames = function() {
        var characterNamesList = [];

        var charactersNamesDOMNodes =
            document.getElementsByClassName('characterName');

        for (var i = 0; i < charactersNamesDOMNodes.length; i++) {
            var currentName =
                charactersNamesDOMNodes[i].innerText.toLowerCase();

            if (characterNamesList.indexOf(currentName) === -1) {
                characterNamesList.push(currentName);
            }
        }

        return characterNamesList;
    };

    var highlightedCharacters = new Set();

    var toggleLineHighlightingForCharacter = function(characterName) {
        if (highlightedCharacters.has(characterName)) {
            highlightedCharacters.delete(characterName);
            unHighlightCharacterLines(characterName);
        } else {
            highlightedCharacters.add(characterName);
            highlightCharacterLines(characterName);
        }
    };

    publicInterface.createCharacterHighlighterButtons =
    createCharacterHighlighterButtons;

    return publicInterface;

})();

var LinesPerCharacterCounter = (function() {

    var publicInterface = {};

    var getCueMap = function() {
        var lineDOMNodes =
            document.getElementsByClassName('line');

        var cueMap = {};

        for (var i = 0; i < lineDOMNodes.length; i++) {
            var characterNameNodes =
            lineDOMNodes[i].getElementsByClassName("characterName");
            var firstChild = characterNameNodes[0];
            {
                if (undefined === cueMap[firstChild.innerText.toLowerCase()]) {
                    cueMap[firstChild.innerText.toLowerCase()] = 1;
                } else {
                    cueMap[firstChild.innerText.toLowerCase()]++;
                }
            }
        }

        return cueMap;

    }

    var buildCueCountSection = function(id) {
        var cueCountSection = document.getElementById(id);

        if (!cueCountSection) {
            console.error("building cue count section failed; " +
                          "I need a div wth id " + id + " in this script");

            return;
        }

        var cueMap = getCueMap();

        for (var characterName in cueMap) {
            if (!cueMap.hasOwnProperty(characterName)) continue;

            var characterNameNode = document.createElement('span');
            characterNameNode.className = "characterName";
            characterNameNode.innerText = characterName;

            var charCueCountNode =
                document.createTextNode(": " + cueMap[characterName]);

            var cueCountItem = document.createElement('div');
            cueCountItem.className = "cueCountItem";
            cueCountItem.appendChild(characterNameNode);
            cueCountItem.appendChild(charCueCountNode);

            cueCountSection.appendChild(cueCountItem);
        }
    }

    publicInterface.buildCueCountSection = buildCueCountSection;

    return publicInterface;
})();

var improveScript = function() {
    LineNumberer.numberLines();
    HighlightButtonMaker.createCharacterHighlighterButtons();
    LinesPerCharacterCounter.buildCueCountSection("cueCounts");
};

document.onload = improveScript();